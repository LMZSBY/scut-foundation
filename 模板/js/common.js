var Common = {
    init: function () {
        this.bind();
    },
    bind: function () {
        Common.siteMap();
        Common.hamburgerMenu();
        Common.showBg();
    },
    siteMap: function () {
        var $body = $('body');
        $('#bar').click(function () {
            var $this = $(this);
            if ($body.hasClass('open_side_bar')) {
                $body.removeClass('open_side_bar');
                $this.removeClass('open');
            } else {
                $body.addClass('open_side_bar');
                $this.addClass('open');
            }
        });
    },
    hamburgerMenu: function () {

        //遮罩
        $('#side_mask').click(function () {
            $('body').removeClass('open_side_bar').removeClass('open_nav');
            $('#bar').removeClass('open');
            $('.hamburger').removeClass('open');
        });
        var $body = $('body');
        var $nav_ul_li = $('nav >ul >li');
        $nav_ul_li.bind({
            mouseenter: function (e) {
                $(this).find('.sub-menu').css('display', 'block');
            },
            mouseleave: function (e) {
                $(this).find('.sub-menu').css('display', 'none');
            }
        });

        //汉堡菜单
        $('.hamburger').click(function () {
            $nav_ul_li.unbind('mouseenter').unbind('mouseleave'); //解绑
            var $this = $(this);
            if ($body.hasClass('open_nav')) {
                $body.removeClass('open_nav');
                $this.removeClass('open');
                $body.css('overflow', 'inherit');
            } else {
                $body.addClass('open_nav');
                $this.addClass('open');

                $body.css('overflow', 'hidden');
            }
        });
    },
    showBg: function () {
        var $nav_ul_li = $(".header-menu >ul >li");
        $nav_ul_li.each(function (i) {
            $(this).hover(
                function () {
                    var $this = $nav_ul_li.eq(i);
                    if ($this.find('>ul >li').length > 0) {
                        $('.header-menu').addClass('show_bg_nav');
                    } else {
                        $('.header-menu').removeClass('show_bg_nav');
                    }
                },
                function () {
                    if ($('.header-menu').hasClass('show_bg_nav')) {
                        $('.header-menu').removeClass('show_bg_nav');
                    }
                }
            );
        });
    },
};
Common.init();