var Common = {
    init: function () {
        this.bind();
    },
    bind: function () {
        Common.siteMap();
        Common.hamburgerMenu();
        Common.showBg();
    },
    siteMap: function () {
        var $body = $('body');
        $('#bar').click(function () {
            var $this = $(this);
            if ($body.hasClass('open_side_bar')) {
                $body.removeClass('open_side_bar');
                $this.removeClass('open');
            } else {
                $body.addClass('open_side_bar');
                $this.addClass('open');
            }
        });
    },
    hamburgerMenu: function () {

        //遮罩
        $('#side_mask').click(function () {
            $('body').removeClass('open_side_bar');
            $('#bar').removeClass('open');
            $('.hamburger').removeClass('open');
        });
        var $body = $('body');

        //汉堡菜单
        $('.hamburger').click(function () {
            var $this = $(this);
            if ($body.hasClass('open_nav')) {
                $body.removeClass('open_nav')
                $this.removeClass('open')
                $body.css("overflow", "inherit")
            } else {
                $body.addClass('open_nav');
                $this.addClass('open')
                $body.css("overflow", "hidden")
            }
        });
        // 手机版展开导航

        $plus = $('.plus');
        $plus.each(function (i) {
            $(this).click(function () {
                if ($(this).parent().hasClass('openPhoneNav')) {
                    $(this).parent().removeClass('openPhoneNav')
                    $(this).addClass('plus').removeClass('minus')
                    $plus.eq(i).siblings('.sub-menu').slideUp(400)
                    $plus.eq(i).siblings().find('._left-list').slideUp(400)
                } else {
                    $(this).parent().siblings().removeClass('openPhoneNav');
                    $(this).parent().addClass('openPhoneNav')
                    $plus.eq(i).siblings('.sub-menu').slideDown(400)
                    $(this).removeClass('plus').addClass('minus')
                    $plus.eq(i).siblings().find('._left-list').slideDown(400)
                    //$(this).parent().siblings().find('._left-list,.sub-menu').slideUp(400)
                    //$(this).parent().siblings().find('.minus').addClass('plus').removeClass('minus')
                }
            })
        });
    },
    showBg: function () {
        var $nav_ul_li = $(".header-menu >ul >li");
        $nav_ul_li.each(function (i) {
            $(this).hover(
                function () {
                    var $this = $nav_ul_li.eq(i);
                    if ($this.find('>ul >li').length > 0) {
                        $('.header-menu').addClass('show_bg_nav');
                    } else {
                        $('.header-menu').removeClass('show_bg_nav');
                    }
                },
                function () {
                    if ($('.header-menu').hasClass('show_bg_nav')) {
                        $('.header-menu').removeClass('show_bg_nav');
                    }
                }
            );
        });
    },
};
Common.init();

//隐藏导航箭头
$('.header-menu > ul > li').each(function () {
    if ($(this).find('ul').length == 0) {
        $(this).find('.plus').css('display', 'none');
    }
})